all: main.pdf

main.pdf:

%.pdf: %.tex
	latexmk -pdf $<


clean:
	latexmk -c

clean-all: clean-img
	latexmk -C


.PHONY: all clean clean-all
